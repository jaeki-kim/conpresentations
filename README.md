# ConPresentations
Micellaneous slide decks, resources, and recordings of some of my conference presentations, invited talks, webinar.

## Conference Presentations
* PASCON(Public institution Affiliated organization Information Security Conference) 2024: Partnering Up, Locking Down: The Partnership Paradox in Cyber Threat Intelligence  (2024.09.)
    + [Slides](https://www.dailysecu.com/form/html/pascon/image/2024/pascon_2024_B-4.pdf)

* ISCR(International Symposium on Cybercrime Response) 2024: Uncovering Evidence in the Shadows of the Dark Web: Reveal The Onion (2024.08.)
    + [Slides]
    + [Page](https://iscr.cyber.go.kr/eng/sub/sub2/sub2_3.html)

* Virus Bulletin 2021: "Operation Newton: Hi Kimsuky? Did an Apple(seed) really fall on Newton’s head?" (1st author)
    + [Slides](https://vblocalhost.com/uploads/2021/09/VB2021-18.pdf)
    + [Paper](https://vblocalhost.com/uploads/VB2021-Kim-etal.pdf)

* Virus Bulletin 2019: "Kimsuky group: tracking the king of the spear-phishing" (1st author)
    + [Slides](https://www.virusbulletin.com/uploads/pdf/conference_slides/2019/VB2019-Kim.pdf)
    + [Paper](https://www.virusbulletin.com/virusbulletin/2020/03/vb2019-paper-kimsuky-group-tracking-king-spearphishing/)

* Blackhat Asia 2019: "When Voice Phishing met Malicious Android App" (3rd author)
    + [Slides](https://i.blackhat.com/asia-19/Fri-March-29/bh-asia-Jang-When-Voice-Phishing-Met-Malicious-Android-App-updated.pdf)

* Virus Bulletin 2018: "DOKKAEBI: Documents of Korean and Evil Binary" (1st author)
    + [Slides](https://www.virusbulletin.com/uploads/pdf/conference_slides/2018/KimKwakJang-VB2018-Dokkaebi.pdf)
    + [Personal Blog](https://jack2.notion.site/Virus-Bulletin-2018-DOKKAEBI-98b2d00c14714fc395f7e232eae8537e)

* ISCR(International Symposium on Cybercrime Response) 2018: Profiling malicious codes using Hangul documents
    + [Blog](https://blog.naver.com/polinlove2/221352234535)

## Invited Talks
* "Unveiling the Shadows: The Intricate Connection Between Ransomware and the Deep & Dark Web" (2nd Ransomware Resilience Conference - Korea Internet & Security Agency, 2023.09.)
    + [Slides](./2023/20230912_Unveiling_the_Shadows_-_The_Intricate_Connection_Between_Ransomware_and_the_Deep_Dark_Web_JACK2.pdf)

* "2022 Threat Issue Overview through Automation Code and Data Analysis (feat. Jupyter Notebook)" (Malware Analysis Working-level Council - Korean Financial Security Institute, 2022.12.)
    + [Slides](https://docs.google.com/presentation/d/e/2PACX-1vQZlh-CYQdu5wRChbWGkrhCNZivhO9XdyyWBLdfcLUynV0nKTBfLiRz4HCAZC865qDkKus97ZVwtC6e/pub?start=false&loop=false&delayms=3000)

* "Tracing virtual asset money laundering and employing state-of-the-art analysis techniques" (Korean National Police, 2022.11.)
    + [Slides](/2022/221103_Tracing%20virtual%20asset%20money%20laundering%20and%20employing%20state-of-the-art%20analysis%20techniques_JACK2.pdf)

* "Analysis of adversary trends and response strategies utilizing the Dark Web" (Republic of Korea Joint Chiefs of Staff, 2022.11.)
    + [Slides](/2022/221117_Analysis%20of%20adversary%20trends%20and%20response%20strategies%20utilizing%20the%20Dark%20Web_JACK2.pdf)

* "Recent Attacks Exploiting Virtual Asset Themes by Kimsuky Group" (Malware Analysis Working-level Council - Korean Financial Security Institute, 2022.06.)
    + [Slides](/2022/220627_Recent%20Attacks%20Exploiting%20Virtual%20Asset%20Themes%20by%20Kimsuky%20Group_JACK2.pdf)

* "Case Study - APT Threat Groups Targeting Law Enforcement" (Malware Analysis Working-level Council - Korean Financial Security Institute, 2021.06.)
    + [Slides](/2021/210629_Case%20Study%20-%20APT%20Threat%20Groups%20Targeting%20Law%20Enforcement_JACK2.pdf)
    + [Blog: Matryoshka : Variant of ROKRAT, APT37 (Scarcruft)](https://medium.com/s2wblog/matryoshka-variant-of-rokrat-apt37-scarcruft-69774ea7bf48)

## Webinar
* S2W WITH: "Utilizing Data Analysis for Actionable Attack Surface Management" (2023.04.)
    + [Slides:p.21~](https://events.s2w.inc/wp-content/uploads/2023/04/S2W_%EC%A0%9C2%ED%9A%8C%EC%9B%A8%EB%B9%84%EB%82%98_%EC%9E%90%EB%A3%8C.pdf)

* S2W Quaxar Launching Day: "The Significance of CTI and Strategies for Leveraging an Internal CTI Organization in Enterprises" (2022.04.)
    + [Slides](https://www.itevents.co.kr/company/S2W/220428_Quaxar_Launching_Day/DATA/Session1_PDF_S2W_QuaxarLaunchingDay_low.pdf)
    + [Videos](https://drive.google.com/file/d/1lBRUvrTx-p1agBgYlCN8leAUIgmQtC4m/view?usp=sharing)